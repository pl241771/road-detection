#imports necessary modules
from flask import Flask, render_template, url_for, redirect, Response
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, login_user, LoginManager, login_required, logout_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired, Length, ValidationError
from flask_bcrypt import Bcrypt
import cv2
import numpy as np
import time

video_path = 'road2.mp4'
image_paths = ['Resources/leftTurnArrow.png', 'Resources/rightTurnArrow.png','Resources/car.png']
file_path = 'logins.txt'
video = cv2.VideoCapture("road2.mp4")
ret, frame = video.read()

ret, frame = video.read()

# cv2.imshow("Video", final)
# cv2.waitkey(1)

#creates all needed servers
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
app.config['SECRET_KEY'] = 'thisisasecretkey'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

#loads sql database
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

#defines the user data structures
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(80), nullable=False)


class RegisterForm(FlaskForm):
    username = StringField(validators=[
                           InputRequired(), Length(min=4, max=20)], render_kw={"placeholder": "Username"})

    password = PasswordField(validators=[
                             InputRequired(), Length(min=8, max=20)], render_kw={"placeholder": "Password"})

    submit = SubmitField('Register')

    def validate_username(self, username):
        existing_user_username = User.query.filter_by(
            username=username.data).first()
        if existing_user_username:
            raise ValidationError(
                'That username already exists. Please choose a different one.')


class LoginForm(FlaskForm):
    username = StringField(validators=[
                           InputRequired(), Length(min=4, max=20)], render_kw={"placeholder": "Username"})

    password = PasswordField(validators=[
                             InputRequired(), Length(min=8, max=20)], render_kw={"placeholder": "Password"})

    submit = SubmitField('Login')

# Loads the image templates
templates = []
template_sizes = []
template_names = []
for image_path in image_paths:
    template = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    templates.append(template)
    template_sizes.append(template.shape)
    template_names.append(image_path.split('/')[-1])


# Define the threshold for matching
threshold = 0.75

#     @app.route('/')
#     def index():
#         return render_template('index.html')

@app.route('/video_feed')
def video_feed():
    video = cv2.VideoCapture("Resources/road2.mp4")
    def detect():
        lastTime=0
        rightCompass = cv2.imread('Resources/rightCompass.png')
        leftCompass = cv2.imread('Resources/leftCompass.png')
        forwardCompass = cv2.imread('Resources/forwardCompass.png')
        direction = None
        while video.isOpened():
            # Read the current frame
            ret, frame = video.read()

            if not ret:
                break

            # Convert the frame to grayscale
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            detected_images = []

            for template, template_size, template_name in zip(templates, template_sizes, template_names):
                # Perform template matching
                result = cv2.matchTemplate(gray, template, cv2.TM_CCOEFF_NORMED)

                # Find matches above the threshold
                locations = np.where(result >= threshold)

                # Draw bounding boxes around the matched regions
                for loc in zip(*locations[::-1]):
                    cv2.rectangle(frame, loc, (loc[0] + template_size[1], loc[1] + template_size[0]), (0, 255, 0), 2)
                    detected_images.append(template_name)

            # Display the resulting frame with detected words
            for detected_image in detected_images:
                if detected_image == 'rightTurnArrow.png':
                    direction="right"
                    lastTime=time.time()
                elif detected_image == 'leftTurnArrow.png':
                    direction="left"
                    lastTime=time.time()
                elif detected_image == 'car.png':
                    frame = cv2.circle(frame, (45,135), 35, (0,0,255), 3)
                    frame = cv2.line(frame, (45,110), (45,145), (0,0,255), 2)
                    frame = cv2.circle(frame, (45,160), 1, (0,0,255), 2)

            if lastTime+3.5 > time.time() and direction != None:
                if direction == "right":
                    start_point = (10, 45) 
                    end_point = (90, 45) 
                    color = (0, 255, 0) 
                    thickness = 6
                    frame = cv2.arrowedLine(frame, start_point, end_point, color, thickness)
                    
                elif direction == "left":
                    start_point = (90, 45) 
                    end_point = (10, 45) 
                    color = (0, 255, 0) 
                    thickness = 6
                    frame = cv2.arrowedLine(frame, start_point, end_point, color, thickness)

            else:
                start_point = (40, 90) 
                end_point = (40, 10) 
                color = (0, 255, 0) 
                thickness = 6
                frame = cv2.arrowedLine(frame, start_point, end_point, color, thickness)
                
            # Convert the frame to grayscale
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Apply Canny edge detection to find edges
            edges = cv2.Canny(gray, 50, 150, apertureSize=3)

            # Perform Hough Line Transform to detect lines
            lines = cv2.HoughLines(edges, 1, np.pi / 180, threshold=210)

            if lines is not None:
                for rho, theta in lines[:, 0, :]:
                    # Convert polar coordinates to Cartesian coordinates
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a * rho
                    y0 = b * rho

                    # Calculate endpoints of the line to draw
                    x1 = int(x0 + 1000 * (-b))
                    y1 = int(y0 + 1000 * (a))
                    x2 = int(x0 - 1000 * (-b))
                    y2 = int(y0 - 1000 * (a))

                    # Draw the line on the frame
                    cv2.line(frame, (x1, y1), (x2, y2), (0, 0, 255), 2)

            # Convert the frame to JPEG format
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            # cv2.waitKey(8)

            # Yield the frame for displaying in the HTML page
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

    return Response(detect(), mimetype='multipart/x-mixed-replace; boundary=frame')
    

#defines API endpoints
@app.route('/')
def home():
    return render_template('home.html')

#returns login page
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user)
                Func = open("static/logins.txt","a")
                Func.write(str(user.username) + " logged in\n")
                Func.close()
                return redirect(url_for('dashboard'))
    return render_template('login.html', form=form)

#returns main page
@app.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    return render_template('dashboard.html')

#logs out
@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

#returns register page
@ app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data)
        new_user = User(username=form.username.data, password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))

    return render_template('register.html', form=form)

if __name__ == "__main__":
    app.run(debug=True)